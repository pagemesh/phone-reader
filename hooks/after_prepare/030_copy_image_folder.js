#!/usr/bin/env node

var ncp = require('ncp').ncp;

ncp.limit = 16;

var source = 'www/img/res';
var destination = 'platforms/android/res';

ncp(source, destination, function (err) {
    if (err) {
        return console.error(err);
    }
    console.log('Files have been copied from ' + source + ' to ' + destination);
});