angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $rootScope, $ionicModal, $timeout, $ionicActionSheet, engine, $location, $window, $state, auth, store) {

        $scope.logout = function() {
            console.log('logout');
            auth.signout();
            store.remove('profile');
            store.remove('token');
            $location.path('login');
        };

        // --- Action menu
        // Triggered on a button click, or some other target
        $scope.show = function () {

            // Show the action sheet
            var hideSheet = $ionicActionSheet.show({
                buttons: [
                    {text: 'Search'},
                    {text: 'Browse'},
                    {text: 'Log out'}
                ],
                titleText: 'Actions',
                cancelText: 'Cancel',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    if(index == 1) $state.go('app.storylines');
                    if(index == 2) $scope.logout();
                    return true;
                }
            });

            // For example's sake, hide the sheet after two seconds
            $timeout(function () {
                hideSheet();
            }, 2000);

        };

        $scope.styles = [
            {name: "action"},
            {name: "adventure"},
            {name: "horror"},
            {name: "thriller"},
            {name: "suspense"},
            {name: "fantastic"},
            {name: "comedy"},
            {name: "adult"}
        ];

    })

    .controller('storylinesCtrl', function ($rootScope, $scope, storylines) {
        $scope.storylines = storylines;
        console.dir($scope.storylines);

        _(storylines.data).forEach(function (storyline) {
            storyline.words = 0;
            _(storyline.scenes).forEach(function (scene) {
                storyline.words += scene.stats.word_count;
            });
        });

        $rootScope.toKeep = [];
        for (var i = 0; i < $scope.storylines.length; i++) $rootScope.toKeep.push(i); // all storylines should be included at beginning

        $scope.selection = {};

        $scope.filter = function (param) {
            console.log("filter storylines");

            var genres = [];
            _($scope.selection).forEach(function (value, key) {
                if (value) genres.push(key);
            });

            for (var i = 0; i < $scope.storylines.length; i++) $rootScope.toKeep.push(i); // all storylines should be included at beginning

            if(param == -1){ $scope.selection = {}; return }
            if (!genres.length) return; // avoid filtering storylines with no tag

            $rootScope.toKeep = [];
            _($scope.storylines).forEach(function (storyline, key) {
                _(storyline.doc.tags).forEach(function (tag) {
                    _(genres).forEach(function (genre) {
                        if (tag.type == "genre" && genre == tag.value) $rootScope.toKeep.push(key);
                    });
                });
            });
        }

    })

    .controller('storylineCtrl', function ($scope, $rootScope, $ionicSlideBoxDelegate, $ionicModal, storyline) {

        $scope.storyline = storyline.doc;

        $scope.storyline.pages = [];
        $scope.storyline.currentPage = 1;

        $scope.storyline.text = "";
        _($scope.storyline.scenes).forEach(function (scene) {
            $scope.storyline.text += scene.content;
        });

        console.dir($scope.storyline);

        $ionicSlideBoxDelegate.slide(0);

        $scope.previousPage = function () {
            $ionicSlideBoxDelegate.previous();
        }

        $scope.nextPage = function () {
            $ionicSlideBoxDelegate.next();
        }

        $scope.slideChanged = function (index) {
            $scope.storyline.currentPage = index + 1;
        }

        $scope.updatePagesContent = function () {
            setTimeout(function () {
                $scope.storyline.pages = breakTextToPages($scope.storyline.text);
                $scope.storyline.totalPage = $scope.storyline.pages.length;
                $ionicSlideBoxDelegate.update();
            }, 10);

        }

        function breakTextToPages(text) {
            var pages = [];
            maxCharPerPage = maxCharactersInWindow(),
                numPages = text.length / maxCharPerPage;
            var remPos = 0, addPos = 0;
            for (var i = 0; i < numPages; i++) {
                // calculate the max number of caracters in a page and cut at the next word
                //if(i==1) console.log(text.substr(i*maxCharPerPage,maxCharPerPage+text.substr((i+1)*maxCharPerPage,maxCharPerPage).indexOf(' ')));
                addPos ? remPos = addPos : 0;
                var addPos = text.substr((i + 1) * maxCharPerPage, maxCharPerPage).indexOf(' ');
                //console.log('Page %d, remove: %d, add: %d', i+1, remPos, addPos);
                pages[i] = {
                    text: text.substr(i * maxCharPerPage - remPos, maxCharPerPage + addPos + remPos),
                    page: i + 1
                };
            }
            console.log('max number of characters per page: ' + maxCharPerPage);
            return pages;
        }

        function maxCharactersInWindow() {
            var div = document.getElementById("pages_container"),
                style = window.getComputedStyle(div),
                font_size = Number(getComputedStyle(div, "").fontSize.match(/(\d*(\.\d*)?)px/)[1]),
                w = window.innerWidth,
                h = window.innerHeight,
                maxChar = w * h;
            console.log("%s em x %s em = %s em2, font size= %s px", w, h, w * h, font_size);
            return Math.round(w * h / font_size / font_size);

        }

        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function () {
            // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function () {
            // Execute action
        });

    })

    .controller('storylineLeftMenuCtrl', function ($scope, storylines) {
        $scope.storylines = storylines.data;
    })

    .controller('storylineRightMenuCtrl', function ($scope) {

    })

    .controller('homeLeftMenuCtrl', function ($scope) {

    })

    .controller('accountCtrl', function ($scope, $state, $localstorage) {
        $scope.account = $localstorage.getObject("account");
        if($scope.account.credit_card) $scope.account.credit_card = "**** **** **** ****"
        $scope.updateAccount = function (account) {
            $localstorage.setObject("account", account);
            $state.go('app.storylines');
        };
        $scope.cancel = function(){ console.log('ok'); $state.go('app.storylines'); };
    })

    .controller('profileCtrl', function ($scope, $state, $localstorage) {
        $scope.profile = $localstorage.getObject("profile");
        $scope.updateProfile = function (profile) {
            $localstorage.setObject("profile", profile);
            $state.go('app.storylines');
        };
        $scope.cancel = function(){ $state.go('app.storylines'); }
    })

    .controller('setttingsCtrl', function ($scope, $state, $localstorage) {
        $scope.settings = $localstorage.getObject("settings");
        $scope.updateSettings = function (settings) {
            $localstorage.setObject("settings", settings);
            $state.go('app.storylines');
        };
        $scope.cancel = function(){ $state.go('app.storylines'); }
    })
;


