angular.module('starter.controllers')
    .controller('LoginCtrl', ['store', '$scope', '$location', 'auth', function(store, $scope, $location, auth) {

        console.log('Initializing login controller');

        $scope.login = function() {
            console.log('click');
            auth.signin({
                authParams: {
                    scope: 'openid offline_access',
                    device: 'Mobile device'
                }
            }, function(profile, token, accessToken, state, refreshToken) {
                // Success callback
                store.set('profile', profile);
                store.set('token', token);
                store.set('refreshToken', refreshToken);
                $location.path('/');
            }, function(err) {
                console.log('err');
                console.log(err);
            });
        }

    }]);