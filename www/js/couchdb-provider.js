angular.module('app.provider', [])

    .provider('couchbase', function () {

        function Couchbase($http) {
            var url, db;

            var getConnectionString = function() {
                //return url + db + '/';
                return url + '/';
            };
            this.get = function (key) {
                console.log('GET: ' + getConnectionString() + key);
                return $http.get(getConnectionString() + key);
            };
            this.list = function (key) {
                console.log('list = ' + getConnectionString() + key + '/_all_docs?include_docs=true');
                return $http.get(getConnectionString() + key + '/_all_docs?include_docs=true');
                //return $http.get(getConnectionString() + key);
            };
            this.post = function (db, objectToPost) {
                console.log('POST: ' + getConnectionString() + db + ' Body: ' + angular.toJson(objectToPost));
                return $http.post(getConnectionString() + db, objectToPost);
            };
            this.put = function (key, objectToPut) {
                console.log(getConnectionString() + key + ' Body: ' + angular.toJson(objectToPut));
                return $http.put(getConnectionString() + key, objectToPut);
            };
            this.delete = function (key) {
                return $http.delete(getConnectionString() + key);
            };
            this.getUrl = function () {
                return url;
            };
            this.setUrl = function (value) {
                console.log('DB url = ' + value);
                url = value;
            };
            this.getDbName = function () {
                return url;
            };
            this.setDbName = function (value) {
                console.log('DB Name is: ' + value);
                db = value;
            };
            this.createDb = function(db) {
                console.log('Create DB: ' + url + db);
                return $http.put(url + db);
            };
        };

        this.$get = ["$window", "$http", '$timeout', function ($window, $http, $timeout) {
            var couchbase = new Couchbase($http);

            couchbase.isDbRemote = false;
            couchbase.allowRemote = true;
            var remote = 'https://pagemesh.iriscouch.com/';

            if(couchbase.isDbRemote || window.location.hostname){
                couchbase.setUrl(remote);
                couchbase.createDb('storylines');
            }
            else {
                $timeout(function () {
                    if ($window.cblite) {
                        $window.cblite.getURL(
                            function (err, url) {
                                if (err) console.log('Error while getting URL');
                                else {
                                    console.log('url found is: ' + url);
                                    couchbase.setUrl(url);
                                    couchbase.createDb('storylines');
                                }
                            });
                    }
                    else{
                        console.log('Cannot access local database!');
                        if(couchbase.allowRemote){
                            console.log('Setting remote DB');
                            couchbase.setUrl(remote);
                            couchbase.createDb(dbName);
                        }
                    }
                }, 4000);
            }
            return couchbase;
        }];
    });