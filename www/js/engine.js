
angular.module('pagemesh.engine', [])


    .provider('engine', function () {

        globalConfig = {
            baseUrl: 'https://pme.pagemesh.com',
            clientId: '000000000002'
        };

        function Engine($q, $timeout, $http, $localstorage, couchbase) {

            this.getStorylines = function() {
                return couchbase.list('storylines');
            };

            this.isLoggedIn = function () {
                var loggedIn = $localstorage.get('token') !== null;

                if (typeof $rootScope !== 'undefined' && typeof $rootScope.currentUser == 'undefined'  && loggedIn) {
                    this.authenticated = this.getCurrentUser();
                    this.authenticated.then(function (user) {
                        $rootScope.currentUser = user;
                        $rootScope.$broadcast('authenticated', user);
                    });

                }

                return loggedIn;
            };

            this.authorize = function () {
                return true;
            };

            this.logout = function () {
                var defer = $q.defer();
                $timeout(function() {
                    $localstorage.set('token',0);
                    $localstorage.set('user',0);
                    if($rootScope) delete $rootScope.currentUser;
                    delete this.authenticated;
                    console.log("Resolving logout");
                    defer.resolve();
                }.bind(this));
                return defer.promise;
            };

            this.getCurrentUser = function () {
                return $http.get('js/data/storylines.json').then(function(user){
                    user.checkRole = function(role) {
                        return _.contains(user.permissions, role);
                    }
                    return user;
                });
            };

        }

        // Method for instantiating
        this.$get = ['$q', '$timeout', '$http', '$localstorage', 'couchbase',
            function ($q, $timeout, $http, $localstorage, couchbase) {
                return new Engine($q, $timeout, $http, $localstorage, couchbase);
            }
        ];

    });




