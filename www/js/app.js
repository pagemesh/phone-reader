// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'pagemesh.engine', 'ionic.utils', 'app.provider',
    'auth0',
    'angular-storage',
    'angular-jwt'])

    .run(function ($ionicPlatform, couchbase, $rootScope, auth, store, jwtHelper, $location) {

        $ionicPlatform.ready(function () {

            // We add an additional delay to prevent the white screen
            setTimeout(function () {
                if (navigator.splashscreen) navigator.splashscreen.hide();
            }, 4000);

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            couchbase.setDbName('pagemesh');

        });

        // This hooks all auth events to check everything as soon as the app starts
        auth.hookEvents();

        // This events gets triggered on refresh or URL change
        $rootScope.$on('$locationChangeStart', function() {
            if (!auth.isAuthenticated) {
                var token = store.get('token');
                if (token) {
                    if (!jwtHelper.isTokenExpired(token)) {
                        auth.authenticate(store.get('profile'), token);
                    } else {
                        // Either show Login page or use the refresh token to get a new idToken
                        $location.path('/');
                    }
                }
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, authProvider) {
        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                data: {
                    requiresLogin: true
                },
                controller: 'AppCtrl',
                resolve: {
                    storylines: function (engine) {
                        return engine.getStorylines().then(
                            function(res){
                                return res.data.rows;
                            }, function(err){
                                console.dir(err);
                            });
                    }
                }
            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })

            .state('app.storylines', {
                url: "/storylines",
                views: {
                    'menuContent': {
                        templateUrl: "templates/storylines.html",
                        controller: 'storylinesCtrl'
                    },
                    'leftMenu@app': {
                        templateUrl: "templates/home_left_menu.html",
                        controller: "homeLeftMenuCtrl"
                    },
                    'rightMenu@app': {
                        templateUrl: "templates/storylines_right_menu.html",
                        controller: "storylinesCtrl"
                    }
                },
                resolve: {
                    storylines: function (storylines) {
                        return storylines;
                    }
                }
            })

            .state('app.storylines.storyline', {
                url: "/:key",
                views: {
                    'menuContent@app': {
                        templateUrl: "templates/storyline.html",
                        controller: 'storylineCtrl'
                    },
                    'leftMenu@app': {
                        templateUrl: "templates/storyline_left_menu.html",
                        controller: "storylineLeftMenuCtrl"
                    },
                    'rightMenu@app': {
                        templateUrl: "templates/storyline_right_menu.html",
                        controller: "storylineCtrl"
                    }
                },
                resolve: {
                    storylines: function (storylines) {
                        return storylines;
                    },
                    storyline: function ($stateParams, storylines) {
                        console.log("get storyline ", $stateParams.key);
                        return _.find(storylines, function (storyline) {
                            return storyline.doc.key == $stateParams.key;
                        });
                    }
                }
            })

            .state('app.account', {
                url: "/account",
                views: {
                    'menuContent': {
                        templateUrl: "templates/account.html",
                        controller: 'accountCtrl'
                    },
                    'leftMenu@app': {
                        templateUrl: "templates/home_left_menu.html",
                        controller: "homeLeftMenuCtrl"
                    },
                    'rightMenu@app': {
                        templateUrl: "templates/storylines_right_menu.html",
                        controller: "storylinesCtrl"
                    }
                }
            })

            .state('app.profile', {
                url: "/profile",
                views: {
                    'menuContent': {
                        templateUrl: "templates/profile.html",
                        controller: 'profileCtrl'
                    },
                    'leftMenu@app': {
                        templateUrl: "templates/home_left_menu.html",
                        controller: "homeLeftMenuCtrl"
                    },
                    'rightMenu@app': {
                        templateUrl: "templates/storylines_right_menu.html",
                        controller: "storylinesCtrl"
                    }
                }
            })

            .state('app.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        templateUrl: "templates/settings.html",
                        controller: 'setttingsCtrl'
                    },
                    'leftMenu@app': {
                        templateUrl: "templates/home_left_menu.html",
                        controller: "homeLeftMenuCtrl"
                    },
                    'rightMenu@app': {
                        templateUrl: "templates/storylines_right_menu.html",
                        controller: "storylinesCtrl"
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/storylines');

        authProvider.init({
            domain: 'pagemesh.auth0.com',
            clientID: 'i4u7DEopKLrq1jPOspyVHKykTrZIHXYW',
            loginState: 'login'
        });

    })

    .config(function (authProvider, $httpProvider, jwtInterceptorProvider) {

        jwtInterceptorProvider.tokenGetter = function(store, jwtHelper, auth) {
            var idToken = store.get('token');
            var refreshToken = store.get('refreshToken');
            // If no token return null
            if (!idToken || !refreshToken) {
                return null;
            }
            // If token is expired, get a new one
            if (jwtHelper.isTokenExpired(idToken)) {
                return auth.refreshIdToken(refreshToken).then(function(idToken) {
                    store.set('token', idToken);
                    return idToken;
                });
            } else {
                return idToken;
            }
        }

        // Work around to avoid the request OPTIONS to be sent in the browser
        if(!window.location.hostname) $httpProvider.interceptors.push('jwtInterceptor');

    })

